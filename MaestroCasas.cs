﻿using UnityEngine;
using System.Collections;

public class MaestroCasas : MonoBehaviour {
	
	public GameObject casaPrefab1;
	public GameObject casaPrefab2;
	public GameObject casaPrefab3;
	public static int[] casasEstado;// 0: no construido ,1: pasar nivel 1,2: nivel1(cap:2) ,3: pasar nivel2, 4: nivel 2(cap:4), 5: pasar nivel3 ,6:nivel 3(cap:8).   
	public GameObject[] casasPosiciones;
	public GameObject[] casasPosiciones2;
	public GameObject[] casasPosiciones3;
	void Start () {
		casasEstado= new int[4];

	}

	void Update () {
		for(int i=0;i<casasEstado.Length;i++) // corraboracion del estado de la casa seleccionada. 
		{
			if(casasEstado[i]==1)
			{
				Instantiate( casaPrefab1,casasPosiciones[i].transform.position,casasPosiciones[i].transform.rotation);
				casasEstado[i]=2;
			}
			if(casasEstado[i]==3)
			{
				Instantiate( casaPrefab2,casasPosiciones2[i].transform.position,casasPosiciones2[i].transform.rotation);
				casasEstado[i]=4;
			}
			if(casasEstado[i]==5)
			{
				Instantiate( casaPrefab3,casasPosiciones3[i].transform.position,casasPosiciones3[i].transform.rotation);
				casasEstado[i]=6;
			}
		}
	}
}
