﻿using UnityEngine;
using System.Collections;

public class MaestroTorres : MonoBehaviour {
	
	public GameObject torrePrefab;
	public static int[] torresEstado;// 0: no construido ,1: pasar nivel 1,2: nivel1(cap:2) ,3: pasar nivel2, 4: nivel 2(cap:4), 5: pasar nivel3 ,6:nivel 3(cap:8).   
	public GameObject[] torresPosiciones;
	void Start () {
		torresEstado= new int[4];
	}

	void Update () {
		for(int i=0;i<torresEstado.Length;i++) // corraboracion del estado de la casa seleccionada. 
		{
			if(torresEstado[i]==1)
			{
				Instantiate(torrePrefab,torresPosiciones[i].transform.position,torresPosiciones[i].transform.rotation);
				torresEstado[i]=2;
			}
			if(torresEstado[i]==3)
			{
				torresEstado[i]=4;
			}
			if(torresEstado[i]==5)
			{
				torresEstado[i]=6;
			}
		}
	}
}
