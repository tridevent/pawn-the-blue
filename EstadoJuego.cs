﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EstadoJuego : MonoBehaviour {
	public static int experiencia; // experiencia con la que se compra todo
	public static int capacidadPoblacion; // capacidad de poblacion a producir
	public Text experienciaText; // muestra la experiencia en el hud
	public Text capacidadPText; // muestra la capacidad de poblacion en el hud
	public static bool actExperiencia; // variable para refrescar resultado de la experiencia
	public static bool actCapacidadP; // variable para refrescar resultado de la capacidad de poblacion 
	public static bool estadoJuego; // variabla que sirve para el cambio de modo
	public GameObject modoJuego; // modo de juego actual
	public static bool posicionarCamara; // variable que para refrezcar la posicion de la camara
	//public GameObject opcion1; 
	//public GameObject consola;
	public static bool cambio=false; // variable que sirve para el cambio de modo
	public static bool edicion=false; // variable que determina si se esta o no en el modo edicion
	public static bool combate=false;//  variable que determina si se esta o no en el modo combate
	void Awake(){
		modoJuego.GetComponent<ModoEdicion>().enabled=true;
		modoJuego.GetComponent<ModoCombate>().enabled=false;
	}
	void Start () {
		experiencia = 25000;
		capacidadPoblacion = 4;
		ActualizarExperiencia ();
		ActualizarCapacidadP ();
	}
	// Update is called once per frame
	void Update () {
	if(Input.GetButtonUp("Cancel"))
		{
			Application.Quit();
		}
	if(Input.GetKeyUp("c") || estadoJuego==true)
		{
			print ("ACa");
			//estadoJuego=!estadoJuego;
			posicionarCamara=true;
			estadoJuego=false;
			cambio=true;
			if(cambio==true && edicion==true)
			{
				cambio=false;
				edicion=false;
				modoJuego.GetComponent<ModoEdicion>().enabled=true;
				modoJuego.GetComponent<ModoCombate>().enabled=false;
			}
			if(cambio==true && combate==true)
			{
				cambio=false;
				combate=false;
				modoJuego.GetComponent<ModoEdicion>().enabled=false;
				modoJuego.GetComponent<ModoCombate>().enabled=true;
			}

		}
	if(actExperiencia==true) // condicion para actualizar el hud
		{
			actExperiencia=false;
			ActualizarExperiencia ();
		}
		if(actCapacidadP==true)
		{
			actCapacidadP=false;
			ActualizarCapacidadP();
		}
	}
	public void ActualizarExperiencia(){
		experienciaText.text = "Exp: " + experiencia;
	}
	public void ActualizarCapacidadP(){
		capacidadPText.text = "Cap: " + capacidadPoblacion;
	}
}
