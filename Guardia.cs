﻿using UnityEngine;
using System.Collections;

public class Guardia : MonoBehaviour {
	public GameObject target; //objetivo
	internal Animator animator; // sistema de animaciones
	internal NavMeshAgent agent; // sistema de ia 
	public int aleatorio; // objetivo aleatorio
	public int cantidad; // cantidad de objetivos ocupados

	// Use this for initialization
	void Start () {
		ChekearPosicionVacia ();
		IrAlObjetivo ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider Col){
	if(Col.gameObject.tag=="porton")
		{
			transform.position= target.transform.position;
		}

	}
	public void ChekearPosicionVacia (){
		aleatorio = Random.Range (0, 16);
		for( int i=0; i<16; i++)
		{
			if(ModoEdicion.objetivoGuardia [i] == true)
			{
				cantidad++;
			}
		}
		if(cantidad<16)
		{
			do
			{
				aleatorio = Random.Range (0, 16);
			}while(ModoEdicion.objetivoGuardia [aleatorio] == true);
			cantidad=0;
		}
		ModoEdicion.objetivoGuardia [aleatorio] = true;
		target = GameObject.Find ("pos (" + aleatorio + ")");
	}
	public void IrAlObjetivo(){
		animator = GetComponent<Animator>();
		agent = GetComponent<NavMeshAgent>();
		agent.destination = target.transform.position;
	}
	public void Entrar(){
		transform.position= target.transform.position;
	}
}
