﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ModoCombate : MonoBehaviour {
	//public GameObject camara; // variable para controlar la camara
	public GameObject comandante; // variable para controlar al comandante
	public static int estadoComandante; //nivel del comandante ***** 0: nivel1(cap:) ,1: pasar nivel2 ,2: nivel2(cap:2) ,3: pasar nivel3, 4: nivel3(cap:4)
	public float velocidad=15; // velocidad en la que se mueve el comandante
	public Text objetoSeleccionado; // informacion sobre la unidad seleccionada
	public GameObject [] estrellas; // estrellas que muestran en el hud el nivel de la undiad seleccionada

	void Start () {
		objetoSeleccionado.text = "Comandante";
		Camera.main.GetComponent<Camera> ().orthographic = false;
		Camera.main.transform.position = new Vector3 (comandante.transform.position.x, comandante.transform.position.y + 60, comandante.transform.position.z);
		Camera.main.transform.parent= comandante.transform;
		estrellas [0].SetActive (true);
		estrellas [1].SetActive (false);
		estrellas [2].SetActive (false);
	}
	public void CambiarModo(){
		estrellas [0].SetActive (false);
		estrellas [1].SetActive (false);
		estrellas [2].SetActive (false);
		objetoSeleccionado.text = "";
		print ("cambiadoCombate");
		EstadoJuego.combate = false;
		EstadoJuego.edicion = true;
		EstadoJuego.estadoJuego = true;
	}

	void Update () {
		if(EstadoJuego.cambio==true)
		{
			print ("entre");
			CambiarModo();
		}
		if(EstadoJuego.posicionarCamara==true)
		{
			Camera.main.transform.position = new Vector3 (comandante.transform.position.x, comandante.transform.position.y + 60, comandante.transform.position.z-25);
			Camera.main.transform.eulerAngles= new Vector3( 70,0,0); 
			Camera.main.transform.parent= comandante.transform;
			EstadoJuego.posicionarCamara=false;
			objetoSeleccionado.text = "Comandante";
			estrellas [0].SetActive (true);
			estrellas [1].SetActive (false);
			estrellas [2].SetActive (false);
		}
		//*****************************************************************************************
		//codigo para mover la camara con las flechas direccionales
		Vector3 ejex=Input.GetAxis("Horizontal")*transform.right*Time.deltaTime*velocidad;
		Vector3 ejey=Input.GetAxis("Vertical")*transform.forward*Time.deltaTime*velocidad;
		comandante.transform.Translate(ejex+ejey);
		//*****************************************************************************************
		//Comandante
		if(estadoComandante==1)
		{
			estrellas [0].SetActive (true);
			estrellas [1].SetActive (true);
			estrellas [2].SetActive (false);
			estadoComandante=2;
		}
		if(estadoComandante==3)
		{
			estrellas [0].SetActive (true);
			estrellas [1].SetActive (true);
			estrellas [2].SetActive (true);
			estadoComandante=4;
		}
	}
}
