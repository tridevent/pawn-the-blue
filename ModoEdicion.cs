﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ModoEdicion : MonoBehaviour {

	public RaycastHit hit= new RaycastHit(); // rayo de seleccion
	public GameObject seleccion; // objeto seleccionado
	//public Transform camara; //donde controlamos el comportamiento de la camara.
	public Text consola; // donde se muestran los mensajes emergentes.
	public Text opcion1Text;// el sub objeto del boton de opcion en donde cambiamos el texto para las diferentes ocaciones 
	public GameObject opcion1;// boton reutilizable en cada edificio
	public float tiempo; //se utiliza para hacer desaparecer los mensajes emergentes en un tiempo determinado
	public int estadoOpcion; //variable que determina la accion del boton opcion que hay en cada edificio.
	public GameObject [] estrellas; // nivel actual del edificio seleccionado
	public Text objetoSeleccionado; // informacion del objeto seleccionado
	public GameObject luzSeleccion; // luz que resalta el edificio seleccionado
	public float velocidad; // velocidad de la camara
	public Text precio; // precio de subir de nivel el edificio
	public GameObject crearUnidad; // boton de crear unidad en reclutamiento
	public 	Sprite[] unidadesImg; // miniaturas de las unidades
	public int estadoUnidades; // determina el nivel y tipo de las unidades
	public int estadoReclutamiento=1;//Niveles del reclutamiento ***** 0: nivel1 ,1: pasar nivel2 ,2: nivel2 ,3: pasar nivel3, 4: nivel3 
	public GameObject[] unidadesObj;//Tipos de unidades a producir ****  0 : reclutamiento lv1 , 1 : reclutamiento lv2,  2 : reclutamiento lv3
	public Transform[] posicionesObj;//lugares pre-establecidos en donde se ubican los guardias inactivos **** 0 : reclutamiento
	public  static bool[] objetivoGuardia;// estado de las posiones aleatorias de los guardias inactivos
	public GameObject[] asignarPorton; // los 3 slots de guardias que debes cubrir para proteger a tu porton
	public bool[] estadoPorton;
	//public bool[] guardiaOcupado;
	public GameObject[] guardias;
	public GameObject[] almGuardia;// variable auxiliar que se utiliza para la posiciones de los guardias para guardarlos.

	//limite de guardias 16 "inactivos" (3 por cada porton)
	void Start () {
		asignarPorton[0].SetActive(false);
		asignarPorton[1].SetActive(false);
		asignarPorton[2].SetActive(false);
		objetivoGuardia = new bool[16];
		crearUnidad.SetActive (false);
		opcion1.SetActive (false);
		estrellas[0].SetActive(false);
		estrellas[1].SetActive(false);
		estrellas[2].SetActive(false);
		objetoSeleccionado.text = "";
		luzSeleccion.SetActive (false);
	}
	public void CambiarModo(){
		crearUnidad.SetActive (false);
		opcion1.SetActive (false);
		estrellas[0].SetActive(false);
		estrellas[1].SetActive(false);
		estrellas[2].SetActive(false);
		objetoSeleccionado.text = "";
		luzSeleccion.SetActive (false);
		luzSeleccion.GetComponent<Light>().range=25;
		print ("cambiado");
		EstadoJuego.edicion = false;
		EstadoJuego.combate = true;
		EstadoJuego.estadoJuego = true;
	}

	void Update () {
	if(EstadoJuego.cambio==true)
		{
			CambiarModo();
		}
		if(EstadoJuego.posicionarCamara==true)
		{
			Camera.main.GetComponent<Camera> ().orthographic = true;
			Camera.main.transform.position = new Vector3 (transform.position.x, transform.position.y + 116, transform.position.z);
			Camera.main.transform.eulerAngles= new Vector3(45,45,0); 
			Camera.main.transform.parent= null;
			EstadoJuego.posicionarCamara=false;
		}
		if(tiempo+1<Time.time)// tiempo para que desaparezca la consola de pantalla
		{
			consola.text="";
		}
	//*****************************************************************************************
	//codigo para mover la camara con las flechas direccionales
	Vector3 ejex=Input.GetAxis("Horizontal")*transform.right*Time.deltaTime*velocidad;
	Vector3 ejey=Input.GetAxis("Vertical")*transform.forward*Time.deltaTime*velocidad;
	Camera.main.transform.Translate(ejex+ejey,Space.World);
	//*****************************************************************************************
	if(Input.GetMouseButtonUp(0))// sistema de seleccion de edificios
		{
			Ray ray= Camera.main.ScreenPointToRay (Input.mousePosition); 
			if (Physics.Raycast (ray, out hit,10000))
			{
				//*******************************************************************************
				//Codigo para la gestion de casas
				if(hit.collider.gameObject.tag == "casa"){
					crearUnidad.SetActive (false);
					print("estoy seleccionado");
					seleccion = hit.collider.gameObject;
					objetoSeleccionado.text = "Casa";
					opcion1.SetActive (true);// boton que pasa crear o subir de nivel el edificio
					luzSeleccion.SetActive (true); // luz de seleccion
					luzSeleccion.transform.position= seleccion.transform.position; // posicionar la luz en el edificio seleccionado.
					for(int i=0; i< MaestroCasas.casasEstado.Length; i++)
					{
						if(seleccion.name=="PosicionCasa ("+i+")")
						{
							if(MaestroCasas.casasEstado[i]==0)
							{
								opcion1Text.text="Crear casa";
								precio.text="500";
								estadoOpcion=0;// estado del boton de opciones
								estrellas[0].SetActive(false);
								estrellas[1].SetActive(false);
								estrellas[2].SetActive(false);
								luzSeleccion.GetComponent<Light>().range=25;
								//1 estrella
							}
							if(MaestroCasas.casasEstado[i]==2)
							{
								opcion1Text.text="Subir a nivel 2";
								estadoOpcion=1;
								precio.text="4000";
								estrellas[0].SetActive(true);
								estrellas[1].SetActive(false);
								estrellas[2].SetActive(false);
								luzSeleccion.GetComponent<Light>().range=55;
								//1 estrella
							}
							if(MaestroCasas.casasEstado[i]==4)
							{
								opcion1Text.text="Subir a nivel 3";
								estadoOpcion=2;
								precio.text="8000";
								estrellas[0].SetActive(true);
								estrellas[1].SetActive(true);
								estrellas[2].SetActive(false);
								luzSeleccion.GetComponent<Light>().range=55;
								//2 estrellas
							}
							if(MaestroCasas.casasEstado[i]==6)
							{
								opcion1.SetActive (false);
								//estadoOpcion=3;
								consola.text="Casa Completada";
								tiempo= Time.time;
								estrellas[0].SetActive(true);
								estrellas[1].SetActive(true);
								estrellas[2].SetActive(true);
								luzSeleccion.GetComponent<Light>().range=55;
								// 3 estrellas
							}
						}
					}
				}
				//*******************************************************************************
				//Codigo para la gestion de torres
				if(hit.collider.gameObject.tag == "torre"){
					crearUnidad.SetActive (false);
					print("estoy seleccionado");
					seleccion = hit.collider.gameObject;
					objetoSeleccionado.text = "Torre";
					opcion1.SetActive (true);// boton que pasa crear o subir de nivel el edificio
					luzSeleccion.SetActive (true); // luz de seleccion
					luzSeleccion.transform.position= seleccion.transform.position; // posicionar la luz en el edificio seleccionado.
					for(int i=0; i< MaestroTorres.torresEstado.Length; i++)
					{
						if(seleccion.name=="PosicionTorre ("+i+")")
						{
							if(MaestroTorres.torresEstado[i]==0)
							{
								opcion1Text.text="Crear torre";
								estadoOpcion=3;// estado del boton de opciones
								precio.text="1500";
								estrellas[0].SetActive(false);
								estrellas[1].SetActive(false);
								estrellas[2].SetActive(false);
								luzSeleccion.GetComponent<Light>().range=25;
								//1 estrella
							}
							if(MaestroTorres.torresEstado[i]==2)
							{
								opcion1Text.text="Subir a nivel 2";
								estadoOpcion=4;
								precio.text="6000";
								estrellas[0].SetActive(true);
								estrellas[1].SetActive(false);
								estrellas[2].SetActive(false);
								luzSeleccion.GetComponent<Light>().range=55;
								//1 estrella
							}
							if(MaestroTorres.torresEstado[i]==4)
							{
								opcion1Text.text="Subir a nivel 3";
								estadoOpcion=5;
								precio.text="15000";
								estrellas[0].SetActive(true);
								estrellas[1].SetActive(true);
								estrellas[2].SetActive(false);
								luzSeleccion.GetComponent<Light>().range=55;
								//2 estrellas
							}
							if(MaestroTorres.torresEstado[i]==6)
							{
								opcion1.SetActive (false);
								//estadoOpcion=6;
								consola.text="Torre Completada";
								tiempo= Time.time;
								estrellas[0].SetActive(true);
								estrellas[1].SetActive(true);
								estrellas[2].SetActive(true);
								luzSeleccion.GetComponent<Light>().range=55;
								// 3 estrellas
							}
						}
					}
				}
				//*****************************************************************************
				//*******************************************************************************
				//Codigo para la gestion de reclutamiento
				if(hit.collider.gameObject.tag == "reclutamiento"){
					print("estoy seleccionado");
					seleccion = hit.collider.gameObject;
					objetoSeleccionado.text = "Reclutamiento";
					print (objetoSeleccionado.text);
					opcion1.SetActive (true);// boton que pasa crear o subir de nivel el edificio
					luzSeleccion.SetActive (true); // luz de seleccion
					luzSeleccion.transform.position= new Vector3 (seleccion.transform.position.x,seleccion.transform.position.y +5 , seleccion.transform.position.z); // posicionar la luz en el edificio seleccionado.

					//Reclutamiento
					if(estadoReclutamiento==1)
					{
						estrellas [0].SetActive (true);
						estrellas [1].SetActive (false);
						estrellas [2].SetActive (false);
						//estadoReclutamiento=2;
						opcion1Text.text="Subir a nivel 2";
						estadoOpcion=6;
						precio.text="5000";
						luzSeleccion.GetComponent<Light>().range=150;
						crearUnidad.SetActive (true);
						crearUnidad.GetComponent<Image>().sprite= unidadesImg[0]; 
						estadoUnidades=1;
					}
					if(estadoReclutamiento==3)
					{
						estrellas [0].SetActive (true);
						estrellas [1].SetActive (true);
						estrellas [2].SetActive (false);
						//estadoReclutamiento=4;
						opcion1Text.text="Subir a nivel 3";
						estadoOpcion=7;
						precio.text="10500";
						luzSeleccion.GetComponent<Light>().range=150;
						crearUnidad.SetActive (true);
						crearUnidad.GetComponent<Image>().sprite= unidadesImg[1]; 
						estadoUnidades=2;
					}
					if(estadoReclutamiento==5)
					{
						opcion1.SetActive (false);
						//estadoOpcion=8;
						consola.text="Reclutamiento Completado";
						tiempo= Time.time;
						estrellas[0].SetActive(true);
						estrellas[1].SetActive(true);
						estrellas[2].SetActive(true);
						luzSeleccion.GetComponent<Light>().range=150;
						crearUnidad.SetActive (true);
						crearUnidad.GetComponent<Image>().sprite= unidadesImg[2]; 
						estadoUnidades=3;
					}
				}
					//*************************************************	
					//*******************************************************************************
					//Codigo para la gestion de porton
					if(hit.collider.gameObject.tag == "porton"){
						asignarPorton[0].SetActive(true);
						asignarPorton[1].SetActive(true);
						asignarPorton[2].SetActive(true);
						crearUnidad.SetActive (false);
						print("estoy seleccionado");
						seleccion = hit.collider.gameObject;
						objetoSeleccionado.text = "Porton";
						//opcion1.SetActive (true);// boton que pasa crear o subir de nivel el edificio
						luzSeleccion.SetActive (true); // luz de seleccion
						luzSeleccion.transform.position= new Vector3 (seleccion.transform.position.x,seleccion.transform.position.y +5 , seleccion.transform.position.z); // posicionar la luz en el edificio seleccionado.
					}
						//*************************************************
	
				//****************************************************************************************
				if(hit.collider.gameObject.layer !=5 && hit.collider.gameObject.layer !=8)// en caso de deseleccion de una unidad
				{
					LimpiarHUD();
				}
				if(hit.collider.gameObject.layer ==8)
				{
					print ("seleccionable");
				}
				if(hit.collider.gameObject.layer ==5)
				{
					print ("HUD");
				}
				//*********************************************************************************
			}
		}
	}
	//*********************************************************************************************
	public void Edificios(){// cuando se hace click en el boton opcion1
		//Sistema de estado de casas
		if(estadoOpcion==0)
		{
			if(EstadoJuego.experiencia>=500)
			{
				for(int i=0; i< MaestroCasas.casasEstado.Length; i++)
				{
					if(seleccion.name=="PosicionCasa ("+i+")")
					{
						MaestroCasas.casasEstado[i]=1;
						consola.text="Casa Construida";
						opcion1.SetActive (false);
						tiempo= Time.time;// para hacer funcionar el tiempo en donde se apagara el cartel
						EstadoJuego.experiencia-=500;
						EstadoJuego.actExperiencia=true;// actualizar los valores en el hud
						EstadoJuego.capacidadPoblacion+=2;
						EstadoJuego.actCapacidadP=true; // actualizar los valores en el hud
						print(EstadoJuego.experiencia);
					}
				}
			}
			else // en caso de que no haya experiencia suficiente.
			{
				consola.text="No hay suficiente experiencia";
				opcion1.SetActive (false);
				tiempo= Time.time;
			}

		}
		if(estadoOpcion==1)
		{
			if(EstadoJuego.experiencia>=4000)
			{
				for(int i=0; i< MaestroCasas.casasEstado.Length; i++)
				{
					if(seleccion.name=="PosicionCasa ("+i+")")
					{
						MaestroCasas.casasEstado[i]=3;
						consola.text="Actualizado a nivel 2";
						opcion1.SetActive (false);
						tiempo= Time.time;
						EstadoJuego.experiencia-=4000;
						EstadoJuego.actExperiencia=true;
						EstadoJuego.capacidadPoblacion+=2;
						EstadoJuego.actCapacidadP=true;
						print(EstadoJuego.experiencia);
					}
				}
			}
			else
			{
				consola.text="No hay suficiente experiencia";
				opcion1.SetActive (false);
				tiempo= Time.time;
			}
		}
		if(estadoOpcion==2)
		{
			if(EstadoJuego.experiencia>=8000)
			{
				for(int i=0; i< MaestroCasas.casasEstado.Length; i++)
				{
					if(seleccion.name=="PosicionCasa ("+i+")")
					{
						MaestroCasas.casasEstado[i]=5;
						consola.text="Actualizado a nivel 3";
						opcion1.SetActive (false);
						tiempo= Time.time;
						EstadoJuego.experiencia-=8000;
						EstadoJuego.actExperiencia=true;
						EstadoJuego.capacidadPoblacion+=4;
						EstadoJuego.actCapacidadP=true;
						print(EstadoJuego.experiencia);
					}
				}
			}
			else
			{
				consola.text="No hay suficiente experiencia";
				opcion1.SetActive (false);
				tiempo= Time.time;
			}
		}
		//*****************************************************************************************
		//sistema de estados de torres
		if(estadoOpcion==3)
		{
			if(EstadoJuego.experiencia>=1500)
			{
				for(int i=0; i< MaestroTorres.torresEstado.Length; i++)
				{
					if(seleccion.name=="PosicionTorre ("+i+")")
					{
						MaestroTorres.torresEstado[i]=1;
						consola.text="Torre Construida";
						opcion1.SetActive (false);
						tiempo= Time.time;// para hacer funcionar el tiempo en donde se apagara el cartel
						EstadoJuego.experiencia-=1500;
						EstadoJuego.actExperiencia=true;// actualizar los valores en el hud
					}
				}
			}
			else // en caso de que no haya experiencia suficiente.
			{
				consola.text="No hay suficiente experiencia";
				opcion1.SetActive (false);
				tiempo= Time.time;
			}
			
		}
		if(estadoOpcion==4)
		{
			if(EstadoJuego.experiencia>=6000)
			{
				for(int i=0; i< MaestroTorres.torresEstado.Length; i++)
				{
					if(seleccion.name=="PosicionTorre ("+i+")")
					{
						MaestroTorres.torresEstado[i]=3;
						consola.text="Actualizado a nivel 2";
						opcion1.SetActive (false);
						tiempo= Time.time;
						EstadoJuego.experiencia-=6000;
						EstadoJuego.actExperiencia=true;
					}
				}
			}
			else
			{
				consola.text="No hay suficiente experiencia";
				opcion1.SetActive (false);
				tiempo= Time.time;
			}
		}
		if(estadoOpcion==5)
		{
			if(EstadoJuego.experiencia>=15000)
			{
				for(int i=0; i< MaestroTorres.torresEstado.Length; i++)
				{
					if(seleccion.name=="PosicionTorre ("+i+")")
					{
						MaestroTorres.torresEstado[i]=5;
						consola.text="Actualizado a nivel 3";
						opcion1.SetActive (false);
						tiempo= Time.time;
						EstadoJuego.experiencia-=15000;
						EstadoJuego.actExperiencia=true;
					}
				}
			}
			else
			{
				consola.text="No hay suficiente experiencia";
				opcion1.SetActive (false);
				tiempo= Time.time;
			}
		}
		//**************************************************************************************************************
		//sistemas de reclutamiento
		if(estadoOpcion==6)
		{
			if(EstadoJuego.experiencia>=5000)
			{
				estadoReclutamiento=3;
				consola.text="Actualizado a nivel 2";
				opcion1.SetActive (false);
				tiempo= Time.time;
				EstadoJuego.experiencia-=5000;
				EstadoJuego.actExperiencia=true;
				crearUnidad.SetActive (false);
			}
			else
			{
				consola.text="No hay suficiente experiencia";
				opcion1.SetActive (false);
				crearUnidad.SetActive (false);
				tiempo= Time.time;
			}
		}
		if(estadoOpcion==7)
		{
			if(EstadoJuego.experiencia>=10500)
			{
				estadoReclutamiento=5;
				consola.text="Actualizado a nivel 3";
				opcion1.SetActive (false);
				tiempo= Time.time;
				EstadoJuego.experiencia-=10500;
				EstadoJuego.actExperiencia=true;
				crearUnidad.SetActive (false);
			}
			else
			{
				consola.text="No hay suficiente experiencia";
				opcion1.SetActive (false);
				crearUnidad.SetActive (false);
				tiempo= Time.time;
			}
		}
		//********************************************************************************************************************
	}
	public void Unidades(){
		if(estadoUnidades==1 && EstadoJuego.experiencia>=100)
		{	
			crearUnidad.SetActive(false);
			guardias= new GameObject[guardias.Length+1];
			Instantiate (unidadesObj[0], posicionesObj[0].transform.position, posicionesObj[0].transform.rotation);
			guardias= GameObject.FindGameObjectsWithTag("guardia");
			EstadoJuego.experiencia-=100;
			EstadoJuego.actExperiencia=true;
		}
		if(estadoUnidades==2 && EstadoJuego.experiencia>=100)
		{	
			crearUnidad.SetActive(false);
			guardias= new GameObject[guardias.Length+1];
			Instantiate (unidadesObj[1], posicionesObj[0].transform.position, posicionesObj[0].transform.rotation);
			print ("cree");
			guardias= GameObject.FindGameObjectsWithTag("guardia");
			EstadoJuego.experiencia-=100;
			EstadoJuego.actExperiencia=true;
		}
		if(estadoUnidades==3 && EstadoJuego.experiencia>=100)
		{	
			crearUnidad.SetActive(false);
			guardias= new GameObject[guardias.Length+1];
			Instantiate (unidadesObj[2], posicionesObj[0].transform.position, posicionesObj[0].transform.rotation);
			print ("cree");
			guardias= GameObject.FindGameObjectsWithTag("guardia");
			EstadoJuego.experiencia-=100;
			EstadoJuego.actExperiencia=true;
		}
	}
	public void LimpiarHUD(){
		opcion1.SetActive (false);
		estrellas[0].SetActive(false);
		estrellas[1].SetActive(false);
		estrellas[2].SetActive(false);
		objetoSeleccionado.text = "";
		luzSeleccion.SetActive (false);
		luzSeleccion.GetComponent<Light>().range=25;
		precio.text="";
		crearUnidad.SetActive (false);
		asignarPorton[0].SetActive(false);
		asignarPorton[1].SetActive(false);
		asignarPorton[2].SetActive(false);
	}
	public void EspacioPorton(int espacio){
		if(estadoPorton[espacio]==false)
		{
			for(int i=0;i<16;i++)
			{
				if(guardias[i]!=null)
				{
					almGuardia[espacio]=guardias[i];
					guardias[i]=null;
					almGuardia[espacio].GetComponent<Guardia>().target= asignarPorton[espacio+3];
					almGuardia[espacio].GetComponent<Guardia>().IrAlObjetivo();
					asignarPorton[espacio].GetComponent<Image>().sprite= unidadesImg[0];
					estadoPorton[espacio]=true;
					i=16;// elevamos el valor de i para cortar el for
				}
			}
			//Instantiate (unidadesObj[0], asignarPorton[3].transform.position, asignarPorton[3].transform.rotation);
		}
		else
		{
			estadoPorton[espacio]=false;
			asignarPorton[espacio].GetComponent<Image>().sprite= unidadesImg[3];
			almGuardia[espacio].GetComponent<Guardia>().target= asignarPorton[6];
			almGuardia[espacio].GetComponent<Guardia>().Entrar();
			almGuardia[espacio].GetComponent<Guardia>().ChekearPosicionVacia();
			almGuardia[espacio].GetComponent<Guardia>().IrAlObjetivo();
		}
	}
}
